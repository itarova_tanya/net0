import {AppBar, Container, Toolbar, Typography} from "@mui/material";
import useStyle from "../utils/styles";
import Head from "next/head";
import Sidebar from "./Sidebar";


export default function Layout({children}) {
    const classes = useStyle();
    return (
        <div>
            <Container>
                {children}
            </Container>
            <footer className={classes.footer}>
                <Typography>
                    all right blah blah
                </Typography>
            </footer>
        </div>
    );
};