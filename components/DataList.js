import React from "react"
import {DataGrid} from '@mui/x-data-grid'
import styles from '../utils/styles'

const DataList = ({columns, rows}) => {

    return (
        <div style={{height: '60vh', width: '100%'}}>
            <DataGrid
                className={styles().dataGrid}
                onCellClick={(params, event) => {
                    event.defaultMuiPrevented = true;
                }}
                onRowClick={(params, event)=>{
                    console.log(params)
                }}
                rows={rows}
                columns={columns}
                pageSize={7}
                rowsPerPageOptions={[7]}
                components={{
                }}
            />
        </div>
    )
}

export default DataList
