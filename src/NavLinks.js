import GroupIcon from '@mui/icons-material/Group';
import ListAltIcon from '@mui/icons-material/ListAlt';
import DashboardIcon from '@mui/icons-material/Dashboard';

export const navLinks = [
    {
        name: "Dashboard",
        path: "/admin",
        icon: <DashboardIcon></DashboardIcon>
    },
    {
        name: "Users",
        path: "/admin/users",
        icon: <GroupIcon></GroupIcon>
    },
    {
        name: "Groups",
        path: "/admin/groups",
        icon: <ListAltIcon></ListAltIcon>
    },
    {
        name: "Pages",
        path: "/admin/pages",
        icon: <ListAltIcon></ListAltIcon>
    },
    {
        name: "Items",
        path: "/admin/items",
        icon: <ListAltIcon></ListAltIcon>
    },
    {
        name: "Quizzes",
        path: "/admin/group-page",
        icon: <ListAltIcon></ListAltIcon>
    },

]