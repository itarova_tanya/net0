import {ListItemIcon, ListItemText, MenuItem, Select} from "@mui/material";
import ForwardToInboxIcon from '@mui/icons-material/ForwardToInbox';
import AcUnitIcon from '@mui/icons-material/AcUnit';
import AccessAlarmIcon from '@mui/icons-material/AccessAlarm';


const IconsList = () => {
    return (
        <Select>
            <MenuItem value={<ForwardToInboxIcon/>}>
                <ListItemIcon>
                    <ForwardToInboxIcon/>
                </ListItemIcon>
                <ListItemText primary="Inbox"/>
            </MenuItem>
            <MenuItem>
                <ListItemIcon>
                    <AcUnitIcon/>
                </ListItemIcon>
                <ListItemText primary="Unit"/>
            </MenuItem>
            <MenuItem>
                <ListItemIcon>
                    <AccessAlarmIcon/>
                </ListItemIcon>
                <ListItemText primary="Clock"/>
            </MenuItem>

        </Select>
    )
}


export default IconsList