import {makeStyles} from "@mui/styles";

const useStyle = makeStyles({
    navbar: {
        backgroundColor: '#31CACA',
        '& a': {
            color: '#ffffff',
            marginLeft: 10
        }
    },
    main: {
        minHeight: '80vh',
    },
    footer: {
        textAlign: 'center'
    },
    navLink: {
        '&:hover': {
            color: '#31CACA',
            cursor: 'pointer',
            '& svg': {
                color: '#31CACA'
            },
        }
    },
    active: {
        color: '#31CACA',
        '& svg': {
            color: '#31CACA'
        },
        '& span': {
            color: '#31CACA'
        },
        '&:hover': {
            color: '#31CACA',
        }
    },

    dataGrid: {
        '& div[role=row]:hover': {
            cursor: 'pointer',
            // backgroundColor:'#E2E2E2'
        },
    },

    primaryButtonContained: {
        // color: '#ffffff',
        // backgroundColor: '#31CACA',
        // '&:hover':{
        //     color: '#ffffff',
        //     backgroundColor: '#31CACA',
        // }
    },
    primaryButtonOutlined: {
        color: '#31CACA',
        borderColor: '#31CACA',
        '&:hover':{
            borderColor: '#31CACA',
        }

    },

})

export default useStyle