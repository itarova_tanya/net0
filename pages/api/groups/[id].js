import prisma from "../../../utils/PrismaClient";

async function handleRequest(data, id, type) {
    if (type === 'update') {
        await prisma.group.update({
            where: {
                id: id
            },
            data: data
        })
    }
    if (type === 'delete') {
        await prisma.group.delete({
            where: {
                id: id
            }
        })
    }
    if (type === 'get') {
        return await prisma.group.findUnique({
            where: {
                id: id
            },
            include: {
                groupPage: {
                    include: {
                        page: true
                    }
                }
            }
        })
    }
    return await prisma.group.findMany();
}

export default async function handler(req, res) {
    if (req.method === "PUT") {
        const id = parseInt(req.query.id, 10)
        const res = await handleRequest(req.body, id, 'update')
        res.status(200).json(res)
    }
    if (req.method === "DELETE") {
        const id = parseInt(req.query.id, 10)
        const res = await handleRequest(req.body, id, 'delete');
        res.status(200).json(res)
    }

    if (req.method === "GET") {
        const id = parseInt(req.query.id, 10)

        let data = await handleRequest(req.body, id, 'get');

        // data.groupPage.map(page => {
        //     page.items.map(async item => {
        //         item.info = await getItem(item.id)
        //     })
        // })
        res.status(200).json(data)
    }
}

async function getItem(id) {
    return await prisma.item.findUnique({
        where: {
            id: id
        },
        include: {
            type: true,
        }
    });

}