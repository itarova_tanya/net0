import prisma from '/utils/PrismaClient'

async function create(data) {
    await prisma.group.create({
        data: data
    })
    return await prisma.group.findMany();
}

export default async function handler(req, res) {
    if (req.method === "POST") {
        const response = await create(req.body)
        res.status(200).json(response)
    }
    if (req.method === "GET") {
        const data = await prisma.group.findMany();
        res.status(200).json(data)

    }
}