import prisma from '/utils/PrismaClient'

async function create(data) {
    await prisma.page.create({
        data: data
    })
    return await prisma.page.findMany();
}

export default function handler(req, res) {
    if (req.method === "POST") {
        create(req.body).then((response)=>{
            res.status(200).json(response)
        })
    }
}