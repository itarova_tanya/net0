import prisma from "../../../utils/PrismaClient";

async function handleRequest(data, id, type) {
    if (type === 'update') {
        await prisma.page.update({
            where: {
                id: id
            },
            data: data
        })
    }
    if (type === 'delete') {
        await prisma.page.delete({
            where: {
                id: id
            }
        })
    }
    return await prisma.page.findMany();
}

export default function handler(req, res) {
    if (req.method === "PUT") {
        const id = parseInt(req.query.id, 10)
        handleRequest(req.body, id, 'update').then((response) => {
            res.status(200).json(response)
        })
    }
    if (req.method === "DELETE") {
        const id = parseInt(req.query.id, 10)
        handleRequest(req.body, id, 'delete').then((response) => {
            res.status(200).json(response)
        })
    }
}
