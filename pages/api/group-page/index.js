import prisma from '/utils/PrismaClient'

async function create(data) {
    await prisma.groupPage.create({
        data: {
            groupId: parseInt(data.groupId, 10),
            pageId: parseInt(data.pageId, 10),

        }
    })
    return await prisma.groupPage.findMany({
        include: {
            group: true,
            page: true,
        },
    });
}

async function list() {
    return await prisma.groupPage.findMany({
        include: {
            group: true,
            page: true,
        },
    });
}

export default function handler(req, res) {
    console.log(req.method)
    switch (req.method) {
        case "POST":
            create(req.body).then((response) => {
                res.status(200).json(response)
            });
            break;
        case "GET":
            list().then((response) => {
                res.status(200).json(response)
            });
            break;
        default:
            res.status(200).json();
            break;
    }


}