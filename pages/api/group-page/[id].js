import prisma from "../../../utils/PrismaClient";

async function handleRequest(data, id, type) {
    if (type === 'update') {
        await prisma.groupPage.update({
            where: {
                id: id
            },
            data: {
                groupId: parseInt(data.groupId, 10),
                pageId: parseInt(data.pageId, 10),

            }
        })
    }
    // if (type === 'delete') {
    //     await prisma.groupPage.delete({
    //         where: {
    //             id: id
    //         }
    //     })
    // }
    if (type === 'get') {
        return await prisma.groupPage.findMany({
            where: {
                id: id
            }
        })
    }
    return await prisma.groupPage.findMany({
        include: {
            group: true,
            page: true,
        },
    });
}

export default function (req, res) {
    if (req.method === "PUT") {
        const id = parseInt(req.query.id, 10)
        handleRequest(req.body, id, 'update').then((response) => {
            res.status(200).json(response)
        })
    }
    // if (req.method === "DELETE") {
    //     const id = parseInt(req.query.id, 10)
    //     handleRequest(req.body, id, 'delete').then((response) => {
    //         res.status(200).json(response)
    //     })
    // }

    if (req.method === "GET") {
        const id = parseInt(req.query.id, 10)
        handleRequest(req.body, id, 'get').then((response) => {
            res.status(200).json(response)
        })
        // res.status(200).json()
    }

}

async function getItem(id) {
    return await prisma.item.findUnique({
        where: {
            id: id
        },
        include: {
            type: true,
        }
    });

}
