import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import prisma from '../../../utils/PrismaClient'

/* JWT secret key */
const KEY = "cdcjdqwidyoisoqwiiqoiwdwqudyqwdjfiewi";

async function getUser(email) {
    return await prisma.user.findUnique({
        where: {
            email: email
        }
    });
}


export default (req, res) => {
    const {method} = req;
    if (method === 'POST') {
        /* Get Post Data */
        const {email, password} = req.body;
        /* Any how email or password is blank */
        if (!email || !password) {
            res.status(400).json({
                status: 'error',
                error: 'Request missing username or password',
            });
        }

        /* Check user email in database */
        try {
            getUser(email).then((response) => {
                const user = response;
                /* Check if exists */
                if (!user) {
                    res.status(400).json({status: 'error', error: 'User Not Found'});
                }
                if (user) {
                    const userId = user.id,
                        userEmail = user.email,
                        userPassword = user.password,
                        userCreated = user.createdAt,
                        userIsAdmin = user.isAdmin;
                    /* Check and compare password */
                    bcrypt.compare(password, userPassword).then(isMatch => {
                        /* User matched */
                        if (isMatch) {
                            /* Create JWT Payload */
                            const payload = {
                                id: userId,
                                email: userEmail,
                                createdAt: userCreated,
                                isAdmin: userIsAdmin,
                            };
                            /* Sign token */
                            jwt.sign(
                                payload,
                                KEY,
                                {
                                    expiresIn: 31556926, // 1 year in seconds
                                },
                                (err, token) => {
                                    /* Send succes with token */
                                    res.status(200).json({
                                        success: true,
                                        token: 'Bearer ' + token,
                                    });
                                },
                            );
                        } else {
                            /* Send error with message */
                            res.status(400).json({status: 'error', error: 'Password incorrect'});
                        }
                    });
                }
            });
        } catch (error) {
            res.status(400).json({
                status: 'error',
                error: error,
            });
        }
    }


}