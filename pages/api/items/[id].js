import prisma from "../../../utils/PrismaClient";

async function handleRequest(data, id, type) {
    if (type === 'update') {
        await prisma.item.update({
            where: {
                id: id
            },
            data: {
                typeCode: parseInt(data.typeCode, 10),
                text: data.text,
                description: data.description,
            }
        })
    }
    if (type === 'delete') {
        await prisma.item.delete({
            where: {
                id: id
            }
        })
    }
    return await prisma.item.findMany({
        include: {
            type: true,
        }
    });
}

export default function handler(req, res) {
    if (req.method === "PUT") {
        const id = parseInt(req.query.id, 10)
        handleRequest(req.body, id, 'update').then((response) => {
            res.status(200).json(response)
        })
    }
    if (req.method === "DELETE") {
        const id = parseInt(req.query.id, 10)
        handleRequest(req.body, id, 'delete').then((response) => {
            res.status(200).json(response)
        })
    }
}
