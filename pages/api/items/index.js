import prisma from '/utils/PrismaClient'


async function create(data) {
    await prisma.item.create({
        data: {
            typeCode: parseInt(data.typeCode, 10),
            text: data.text,
            description: data.description,
            // type: {
            //     connect: {
            //         code: parseInt(data.typeCode, 10)
            //     }
            // }
        }
    })
    return await prisma.item.findMany({
        include: {
            type: true,
        },
    });
}

export default function handler(req, res) {
    if (req.method === "POST") {
        create(req.body).then((response) => {
            res.status(200).json(response)
        })
    }
}