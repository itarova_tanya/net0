import * as React from 'react';
import {useRouter} from 'next/router'
import {DataGrid, GridToolbar} from '@mui/x-data-grid';
import {Button} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import Sidebar from '../../../components/Sidebar';
import styles from "../../../utils/styles";
import useSWR from 'swr'

/* middleware */
import {
    absoluteUrl,
    getAppCookies,
    verifyToken,
    setLogout,
} from '../../../middleware/utils';

const fetcher = (...args) => fetch(...args).then((res) => res.json())


const GroupPageList = (props) => {
    const router = useRouter();
    const classes = styles();
    const {profile} = props;

    const [loggedAdmin, setLoggedAdmin] = React.useState(false);
    const [logged, setLogged] = React.useState(false);

    const {data, error} = useSWR('/api/group-page', fetcher);

    const columns = [
        {
            field: 'group',
            headerName: 'Group',
            flex: 1,
            editable: false,
            valueGetter: (params) => {
                return params.value.name
            }
        },
        {
            field: 'page',
            headerName: 'Page',
            flex: 1,
            editable: false,
            valueGetter: (params) => {
                return params.value.name
            }
        },
    ];

    React.useEffect(() => {
        if (profile) {
            setLogged(true)
            if (profile.isAdmin) {
                setLoggedAdmin(true)
            }
        } else {
            router.push('/admin/auth/login')
        }
    }, []);

    const handleEdit = (itemId) => {
        router.push('/admin/group-page/' + itemId);
    }

    const handleCreate = () => {
        router.push('/admin/group-page/create');
    }

    function handleOnClickLogout(e) {
        setLogout(e);
    }

    if (error) return <div>Failed to load</div>

    if (!data) return <div>Loading...</div>

    return (
        <>
            {loggedAdmin ? (
                <Sidebar>
                    <div style={{textAlign: "right"}}>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                    <h1>Group Pages</h1>
                    <Button
                        onClick={handleCreate}
                        variant="outlined"
                        startIcon={<AddIcon/>}
                    > new Quiz </Button>
                    <p></p>
                    <div style={{height: '60vh', width: '100%'}}>
                        <DataGrid
                            className={classes.dataGrid}
                            rows={data}
                            columns={columns}
                            pageSize={7}
                            rowsPerPageOptions={[7]}
                            components={{
                                Toolbar: GridToolbar,
                            }}

                            onRowClick={(params, event) => {
                                handleEdit(params.row.id)
                            }}
                        />
                    </div>

                </Sidebar>
            ) : (
                logged && (
                    <>
                        <h2>403 - Forbidden</h2>
                        <div>
                            <a href="#" onClick={e => handleOnClickLogout(e)}>
                                &bull; Logout
                            </a>
                        </div>
                    </>
                )
            )}
        </>
    )


}
export default GroupPageList

export async function getServerSideProps(context) {
    const {req} = context;

    const {token} = getAppCookies(req);
    const profile = token ? verifyToken(token.split(' ')[1]) : '';

    return {
        props: {
            profile
        }
    }

}