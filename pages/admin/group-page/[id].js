import * as React from 'react';
import {useRouter} from 'next/router'
import {getAppCookies, setLogout, verifyToken} from "../../../middleware/utils";
import prisma from "../../../utils/PrismaClient";
import styles from "../../../utils/styles";
import axios from "axios";
import Sidebar from "../../../components/Sidebar";
import {
    Box,
    Button,
    FormControl,
    Grid,
    InputLabel,
    MenuItem,
    Divider,
    Select,
    Stack, TextField,
    IconButton
} from "@mui/material";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import DeleteIcon from '@mui/icons-material/Delete';


const GroupPage = (props) => {

    const {profile} = props;
    const {groups} = props;
    const {pages} = props;
    const {allItems} = props;
    const {groupPage} = props;

    const router = useRouter()

    const [loggedAdmin, setLoggedAdmin] = React.useState(false);
    const [logged, setLogged] = React.useState(false);
    const [data, setData] = React.useState(groupPage);
    const [group, setGroup] = React.useState(data.group.id);
    const [page, setPage] = React.useState(data.page.id);
    const [pageNumber, setPageNumber] = React.useState(data.pageNumber);
    const [questions, setQuestions] = React.useState(data.items);
    const [id, setId] = React.useState(data.id);


    const handleGroupChange = (event) => {
        setGroup(event.target.value);
    };
    const handlePageChange = (event) => {
        setPage(event.target.value);
    };
    const handlePageNumberChange = (event) => {
        setPageNumber(event.target.value);
    };
    const handleOnClickLogout = (e) => {
        setLogout(e);
    }

    const addNewItemBox = () => {

        let ar;
        if (!questions) {
            ar = []
            setQuestions([{id: '', number: ''}]);
        } else {
            setQuestions([...questions,
                {id: '', number: ''}
            ]);
        }


    }

    const removeItemBox = (key) => {
        if (questions){
            setQuestions(questions.splice(key))
        }
    }

    const handleDeleteGroupPage = () => {
        // TODO
        //     alert then
        axios
            .delete('/api/group-page/' + groupPage.id)
            .then((response) => {
                // TODO
                //     ответ операции удаления (redirect)
            })
    }

    React.useEffect(() => {
        if (profile) {
            setLogged(true)
            if (profile.isAdmin) {
                setLoggedAdmin(true)
            }
        } else {
            router.push('/admin/auth/login')
        }
        if (questions === null) {
            setQuestions([])
        }
    }, [])

    const submitForm = (event) => {
        event.preventDefault();
        const submitData = {
            groupId: event.target.group.value,
            pageId: event.target.page.value,
        }

        axios
            .put('/api/group-page/' + id, submitData)
            .then((response) => {
                setData(response.data)
                alert("Все ок")
            })
    }

    return (
        <>
            {loggedAdmin ? (
                <Sidebar>
                    <div style={{textAlign: "right"}}>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>


                    <h2>Детальный просмотр</h2>

                    <Box
                        sx={{
                            p: 2,
                        }}
                        component="form"
                        onSubmit={submitForm}
                    >

                        <Grid container spacing={2} sx={{marginBottom: 3}}>
                            <Grid item xs={12} md={8}>
                                <FormControl fullWidth>
                                    <InputLabel id="group-select-label">Group</InputLabel>
                                    <Select
                                        id="group"
                                        name="group"
                                        value={group}
                                        label="Group"
                                        onChange={handleGroupChange}
                                    >
                                        {groups.map((group) =>
                                            <MenuItem
                                                key={group.id}
                                                value={group.id}
                                            > {group.name}
                                            </MenuItem>
                                        )}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={8}>
                                <FormControl fullWidth>
                                    <InputLabel id="page-select-label">Page</InputLabel>
                                    <Select
                                        id="page"
                                        name="page"
                                        value={page}
                                        label="Page"
                                        onChange={handlePageChange}
                                    >
                                        {pages.map((page) =>
                                            <MenuItem
                                                key={page.id}
                                                value={page.id}
                                            > {page.name}
                                            </MenuItem>
                                        )}
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} md={8}>
                                <FormControl fullWidth>
                                    <TextField
                                        InputProps={{inputProps: {min: 1}}}
                                        type="number"
                                        id="page-number"
                                        name="page-number"
                                        value={pageNumber}
                                        label="Page Number"
                                        onChange={handlePageNumberChange}
                                    >
                                    </TextField>
                                </FormControl>
                            </Grid>
                        </Grid>

                        <Divider textAlign="left">
                            Items(questions)
                            <IconButton
                                onClick={addNewItemBox}
                            >
                                <AddCircleOutlineIcon/>
                            </IconButton>
                        </Divider>


                        {questions.map((item, key) =>
                            <Grid container spacing={2} key={key} sx={{marginBottom: 2, marginTop: 1}}
                                  id={"item-" + key}>
                                <Grid item xs={12} md={2} sm={3}>
                                    <FormControl fullWidth>
                                        <TextField
                                            InputProps={{inputProps: {min: 1}}}
                                            type="number"
                                            value={item.number}
                                            label="Number"
                                        >
                                        </TextField>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={12} md={6} sm={8}>


                                    <FormControl fullWidth>
                                        <InputLabel id="item-select-label">Item</InputLabel>
                                        <Select
                                            labelId="item-select-label"
                                            id="item"
                                            name="item"
                                            value={item.id}
                                            label="Item"
                                        >
                                            {allItems.map((value) =>
                                                <MenuItem
                                                    key={value.id}
                                                    value={value.id}
                                                > {value.text}
                                                </MenuItem>
                                            )}
                                        </Select>
                                    </FormControl>

                                </Grid>
                                <Grid item xs={12} md={2} sm={1} className="remove-item-from-page-list"
                                      sx={{paddingLeft: 0}}>
                                    <IconButton aria-label="delete"
                                                onClick={() => {
                                                    removeItemBox(key)
                                                }}
                                    >
                                        <DeleteIcon/>
                                    </IconButton>
                                </Grid>
                            </Grid>
                        )}


                        <Stack direction="row" spacing={2}>
                            <Button
                                type="submit"
                                variant="contained"
                                sx={{
                                    backgroundColor: '#31CACA',
                                    '&:hover': {backgroundColor: '#27a2a2'}
                                }}
                            >
                                Save
                            </Button>
                            <Button
                                type="button"
                                onClick={handleDeleteGroupPage()}
                                variant="contained"
                                color={'error'}
                                //     sx={{mt: 3, mb: 2, backgroundColor: '#F33535'}}
                            >
                                Delete
                            </Button>
                        </Stack>
                    </Box>

                </Sidebar>
            ) : (
                logged && (
                    <>
                        <h2>403 - Forbidden</h2>
                        <div>
                            <a href="#" onClick={e => handleOnClickLogout(e)}>
                                &bull; Logout
                            </a>
                        </div>
                    </>
                )
            )}
        </>
    )
}


export default GroupPage

export async function getServerSideProps(context) {
    const {req} = context;

    const {token} = getAppCookies(req);
    const profile = token ? verifyToken(token.split(' ')[1]) : '';


    let groups;
    let pages;
    let groupPage;
    let allItems;

    try {
        groupPage = await prisma.groupPage.findUnique(
            {
                where: {
                    id: parseInt(context.params.id),
                },
                include: {
                    group: true,
                    page: true,
                },
            }
        );
        groups = await prisma.group.findMany();
        pages = await prisma.page.findMany();
        allItems = await prisma.item.findMany();

    } catch (err) {
        console.log(err)
    }

    return {
        props: {
            profile,
            groupPage,
            groups,
            pages,
            allItems
        }
    }

}