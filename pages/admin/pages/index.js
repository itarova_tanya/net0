import Sidebar from "../../../components/Sidebar";
import * as React from 'react';
import {useRouter} from 'next/router'
import prisma from "/utils/PrismaClient"
import AddIcon from "@mui/icons-material/Add";
import {TextField, Box, Button, Typography, Modal} from "@mui/material";
import {DataGrid} from "@mui/x-data-grid";
import styles from "../../../utils/styles";
import axios from "axios";

/* middleware */
import {
    absoluteUrl,
    getAppCookies,
    verifyToken,
    setLogout,
} from '../../../middleware/utils';


const PagesList = (props) => {

    const router = useRouter()

    const classes = styles()
    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '50vw',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 12,
        p: 4,
    };

    const {profile} = props;
    const {pages} = props;

    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            flex: 0.5,
            editable: false,
        },
        {
            field: 'name',
            headerName: 'Name',
            flex: 1,
            editable: false,
        },
        {
            field: 'text',
            headerName: 'Text',
            flex: 1,
            editable: false,
        },
        {
            field: 'description',
            headerName: 'Description',
            flex: 1,
            editable: false,
        },
    ];

    const [loggedAdmin, setLoggedAdmin] = React.useState(false)
    const [logged, setLogged] = React.useState(false)

    const [open, setOpen] = React.useState(false)
    const [modalTitle, setModalTitle] = React.useState(null)
    const [data, setData] = React.useState(pages)
    const [name, setName] = React.useState(null)
    const [text, setText] = React.useState(null)
    const [id, setId] = React.useState(null)
    const [formIsValid, setFormIsValid] = React.useState(true)
    const [formError, setFormError] = React.useState(null)
    const [description, setDescription] = React.useState(null)
    const [submitType, setSubmitType] = React.useState("create")


    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);


    const handleModalSubmit = (event) => {
        event.preventDefault();

        if ((event.target.name.value).trim() && (event.target.description.value).trim() && (event.target.text.value).trim()) {
            switch (submitType) {
                case "update":
                    axios
                        .put('/api/pages/' + id, {
                            name: (event.target.name.value).trim(),
                            text: (event.target.text.value).trim(),
                            description: (event.target.description.value.trim())
                        })
                        .then((response) => {
                            setOpen(false)
                            setData(response.data)
                            setFormIsValid(true)
                        })
                    break
                case "create":
                    axios
                        .post('/api/pages', {
                            name: (event.target.name.value).trim(),
                            text: (event.target.text.value).trim(),
                            description: (event.target.description.value.trim())
                        })
                        .then((response) => {
                            setOpen(false)
                            setData(response.data)
                        })
                    break
                default:
                    break

            }
        } else {
            setFormIsValid(false)
            setFormError("Enter valid values!")
        }

    }
    const handleDelete = (event) => {
        axios
            .delete('/api/pages/' + id)
            .then((response) => {
                setOpen(false)
                setData(response.data)
                setFormIsValid(true)
            })
    }

    function handleOnClickLogout(e) {
        setLogout(e);
    }

    React.useEffect(() => {
        if (profile) {
            setLogged(true)
            if (profile.isAdmin) {
                setLoggedAdmin(true)
            }
        } else {
            router.push('/admin/auth/login')
        }
    }, [])


    return (
        <>

            {loggedAdmin ? (
                <Sidebar>
                    <div style={{textAlign: "right"}}>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                    <h1>Pages List</h1>
                    <Button
                        className={classes.primaryButtonOutlined}
                        onClick={
                            () => {
                                setModalTitle("Add new page")
                                setName(null)
                                setText(null)
                                setDescription(null)
                                setId(null)
                                setSubmitType("create")
                                handleOpen()
                            }
                        }
                        variant="outlined"
                        startIcon={<AddIcon/>}
                    >
                        new Page
                    </Button>
                    <p></p>
                    <Modal
                        open={open}
                        position='absolute'
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Typography id="modal-modal-title" variant="h6" component="h2">
                                {modalTitle}
                            </Typography>

                            {!formIsValid &&
                                <Typography
                                    paragraph={true}
                                    sx={{
                                        color: 'red',
                                        mt: 1,
                                        mb: 0
                                    }}
                                >
                                    {formError}
                                </Typography>
                            }

                            <Box component="form" noValidate
                                 onSubmit={handleModalSubmit}
                            >
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="name"
                                    label="Name"
                                    name="name"
                                    autoFocus
                                    autoComplete="off"
                                    defaultValue={name}
                                />

                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="text"
                                    label="Text"
                                    name="text"
                                    autoComplete="off"
                                    multiline={true}
                                    minRows={2}
                                    defaultValue={text}
                                />
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="description"
                                    label="Description"
                                    name="description"
                                    autoComplete="off"
                                    multiline={true}
                                    minRows={2}
                                    defaultValue={description}
                                />

                                <Button
                                    type="submit"
                                    className={classes.primaryButtonContained}
                                    variant="contained"
                                    sx={{
                                        mt: 3,
                                        mb: 2,
                                        mr: 3,
                                        backgroundColor: '#31CACA',
                                        '&:hover': {backgroundColor: '#27a2a2'}
                                    }}
                                >
                                    Save
                                </Button>
                                {id &&
                                    <Button
                                        onClick={handleDelete}
                                        variant="contained"
                                        color={'error'}
                                        sx={{mt: 3, mb: 2, backgroundColor: '#F33535'}}
                                    >
                                        Delete
                                    </Button>
                                }

                            </Box>
                        </Box>
                    </Modal>

                    <div style={{height: '60vh', width: '100%'}}>
                        <DataGrid
                            className={classes.dataGrid}
                            onCellClick={(params, event) => {
                                event.defaultMuiPrevented = true;
                            }}
                            onRowClick={(params, event) => {
                                setModalTitle("Edit Page")
                                setName(params.row.name)
                                setText(params.row.text)
                                setId(params.row.id)
                                setSubmitType("update")
                                setDescription(params.row.description)
                                handleOpen()
                            }}
                            rows={data}
                            columns={columns}
                            pageSize={7}
                            rowsPerPageOptions={[7]}
                        />
                    </div>

                </Sidebar>


            ) : (
                logged &&
                (<>
                    <h2>403 - Forbidden</h2>
                    <div>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                </>)
            )}
        </>
    )


}

export default PagesList

export async function getServerSideProps(context) {
    const {req} = context;

    const {token} = getAppCookies(req);
    const profile = token ? verifyToken(token.split(' ')[1]) : '';

    try {
        const pages = await prisma.page.findMany()
        return {
            props: {
                pages: JSON.parse(JSON.stringify(pages)),
                profile
            }
        }
    } catch (err) {
        return {props: {profile, pages: []}}
    }

}


