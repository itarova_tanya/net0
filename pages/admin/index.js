import Sidebar from "../../components/Sidebar";
import React from 'react';
import Link from 'next/link';

/* middleware */
import {
    absoluteUrl,
    getAppCookies,
    verifyToken,
    setLogout,
} from '../../middleware/utils';
import {useRouter} from 'next/router'

const Dashboard = (props) => {
    const router = useRouter()
    const {profile} = props;
    const [loggedAdmin, setLoggedAdmin] = React.useState(false)
    const [logged, setLogged] = React.useState(false)
    const [loading, setLoading] = React.useState(false);


    function handleOnClickLogout(e) {
        setLogout(e);
    }

    React.useEffect(() => {
        setLoading(true)
        if (profile) {
            setLogged(true)
            if (profile.isAdmin) {
                setLoggedAdmin(true)
            }
        } else {
            router.push('/admin/auth/login')
        }
        // setLoading(false)
    }, [])

    return (
        <>
            {loggedAdmin ? (
                <Sidebar>
                    <div style={{textAlign: "right"}}>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                    <h1>dashboard</h1>
                </Sidebar>

            ) : (

                logged &&
                <>
                    <h2>403 - Forbidden</h2>
                    <div>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                </>

            )}

        </>
    )
}

export default Dashboard

export async function getServerSideProps(context) {
    const {req} = context;
    const {origin} = absoluteUrl(req);

    const baseApiUrl = `${origin}/api/about`;

    const {token} = getAppCookies(req);
    const profile = token ? verifyToken(token.split(' ')[1]) : '';

    return {
        props: {
            baseApiUrl,
            profile,
        },
    };
}