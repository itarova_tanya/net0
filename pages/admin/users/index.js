import Sidebar from "../../../components/Sidebar";
import DataList from "../../../components/DataList";
import prisma from "/utils/PrismaClient"
import IconButton from "@mui/material/IconButton";
import {AddBox, FormatLineSpacing, SpaceBar} from "@mui/icons-material";
import AddIcon from '@mui/icons-material/Add';
import {Button, ButtonGroup} from "@mui/material";
import * as React from "react";
import {useRouter} from "next/router";


/* middleware */
import {
    absoluteUrl,
    getAppCookies,
    verifyToken,
    setLogout,
} from '../../../middleware/utils';

const UsersList = (props) => {

    const router = useRouter()

    const {profile, users} = props;

    const [loggedAdmin, setLoggedAdmin] = React.useState(false)
    const [logged, setLogged] = React.useState(false)


    function handleOnClickLogout(e) {
        setLogout(e);
    }

    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            flex: 0.5,
        },
        {
            field: 'name',
            headerName: 'Full name',
            flex: 1,
            editable: false,
        },
        {
            field: 'email',
            headerName: 'Email',
            flex: 1,
            editable: false,
        },
    ];

    React.useEffect(() => {
        if (profile) {
            setLogged(true)
            if (profile.isAdmin) {
                setLoggedAdmin(true)
            }
        } else {
            router.push('/admin/auth/login')
        }
    }, [])

    return (
        <>
            {loggedAdmin ? (
                <Sidebar>
                    <div style={{textAlign: "right"}}>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                    <h1>Users List</h1>

                    <Button
                        sx={{mb:1}}
                        variant="outlined"
                        startIcon={<AddIcon/>}
                    >
                        new User
                    </Button>

                    <DataList
                        rows={users}
                        columns={columns}
                    >
                    </DataList>
                </Sidebar>
            ) : (
                logged && (
                    <>
                        <h2>403 - Forbidden</h2>
                        <div>
                            <a href="#" onClick={e => handleOnClickLogout(e)}>
                                &bull; Logout
                            </a>
                        </div>
                    </>
                )
            )}

        </>
    )
}
export default UsersList

export async function getServerSideProps(context) {

    const {req} = context;

    const {token} = getAppCookies(req);
    const profile = token ? verifyToken(token.split(' ')[1]) : '';


    const users = await prisma.user.findMany()

    return {
        props: {
            profile,
            users: JSON.parse(JSON.stringify(users))
        }
    }
}

