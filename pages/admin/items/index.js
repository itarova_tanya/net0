import Sidebar from "../../../components/Sidebar";
import * as React from 'react';
import {useRouter} from 'next/router'
import prisma from "/utils/PrismaClient"
import AddIcon from "@mui/icons-material/Add";
import {Button, Box, Typography, Select, Modal, InputLabel, FormControl, MenuItem, TextField} from "@mui/material";
import {DataGrid} from "@mui/x-data-grid";
import styles from "../../../utils/styles";
import axios from "axios";

import {absoluteUrl, getAppCookies, setLogout, verifyToken} from "../../../middleware/utils";

const ItemsList = (props) => {
    const router = useRouter()

    const classes = styles()

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '50vw',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 12,
        p: 4,
    };

    const {profile} = props;
    const {items} = props;
    const {types} = props;

    const [loggedAdmin, setLoggedAdmin] = React.useState(false)
    const [logged, setLogged] = React.useState(false)
    const [open, setOpen] = React.useState(false)
    const [modalTitle, setModalTitle] = React.useState(null)
    const [data, setData] = React.useState(items)
    const [text, setText] = React.useState(null)
    const [description, setDescription] = React.useState(null)
    const [itemType, setItemType] = React.useState(types[0].code)
    const [id, setId] = React.useState(null)
    const [formIsValid, setFormIsValid] = React.useState(true)
    const [formError, setFormError] = React.useState(null)
    const [submitType, setSubmitType] = React.useState("create")

    React.useEffect(() => {
        if (profile) {
            setLogged(true)
            if (profile.isAdmin) {
                setLoggedAdmin(true)
            }
        } else {
            router.push('/admin/auth/login')
        }
    }, [])

    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            flex: 0.5,
        },
        {
            field: 'text',
            headerName: 'Text',
            flex: 1,
        },
        {
            field: 'description',
            headerName: 'Description',
            flex: 1,
            editable: false,
        },
        {
            field: 'type',
            headerName: 'type code',
            flex: 1,
            // type: 'singleSelect',
            editable: false,
            // 'options': types
            valueGetter: (params) => {
                return params.value.name
            }
        },
    ];

    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const handleChange = (event) => {
        setItemType(event.target.value);
    };

    const handleModalSubmit = (event) => {
        event.preventDefault();

        if ((event.target.description.value).trim() && (event.target.text.value).trim()) {
            switch (submitType) {
                case "update":
                    axios
                        .put('/api/items/' + id, {
                            text: (event.target.text.value).trim(),
                            description: (event.target.description.value).trim(),
                            typeCode: event.target.itemType.value
                        })
                        .then((response) => {
                            setOpen(false)
                            setData(response.data)
                            setFormIsValid(true)
                        })
                    break
                case "create":
                    axios
                        .post('/api/items', {
                            text: (event.target.text.value).trim(),
                            description: (event.target.description.value.trim()),
                            typeCode: (event.target.itemType.value.trim())
                        })
                        .then((response) => {
                            setOpen(false)
                            setData(response.data)
                        })
                    break
                default:
                    break

            }
        } else {
            setFormIsValid(false)
            setFormError("Enter valid values!")
        }

    }

    const handleDelete = () => {
        axios
            .delete('/api/items/' + id)
            .then((response) => {
                setOpen(false)
                setData(response.data)
                setFormIsValid(true)
            })
    }

    function handleOnClickLogout(e) {
        setLogout(e);
    }


    return (
        <>

            {loggedAdmin ? (
                <Sidebar>
                    <div style={{textAlign: "right"}}>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                    <h1>Items</h1>
                    <Button
                        className={classes.primaryButtonOutlined}
                        onClick={
                            () => {
                                setModalTitle("Add new item(question)")
                                setText(null)
                                setDescription(null)
                                setId(null)
                                setSubmitType("create")
                                handleOpen()
                            }
                        }
                        variant="outlined"
                        startIcon={<AddIcon/>}
                    >
                        new Item(question)
                    </Button>
                    <p></p>
                    <Modal
                        open={open}
                        position='absolute'
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Typography id="modal-modal-title" variant="h6" component="h2">
                                {modalTitle}
                            </Typography>

                            {!formIsValid &&
                                <Typography
                                    paragraph={true}
                                    sx={{
                                        color: 'red',
                                        mt: 1,
                                        mb: 0
                                    }}
                                >
                                    {formError}
                                </Typography>
                            }

                            <Box component="form"
                                 onSubmit={handleModalSubmit}
                            >

                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="text"
                                    label="Text"
                                    name="text"
                                    autoComplete="off"
                                    multiline={true}
                                    minRows={1}
                                    defaultValue={text}
                                />
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="description"
                                    label="Description"
                                    name="description"
                                    autoComplete="off"
                                    multiline={true}
                                    minRows={2}
                                    defaultValue={description}
                                />

                                <FormControl fullWidth>
                                    <InputLabel id="item-type-select-label"
                                                sx={{marginTop: '16px', marginBottom: '8px'}}>Type</InputLabel>
                                    <Select
                                        value={itemType}
                                        sx={{marginTop: '16px', marginBottom: '8px'}}
                                        id="itemType"
                                        name="itemType"
                                        label="Type"
                                        onChange={handleChange}
                                    >
                                        {types.map((type) =>
                                            <MenuItem
                                                key={type.id}
                                                value={type.code}
                                            > {type.name}
                                            </MenuItem>
                                        )}

                                    </Select>
                                </FormControl>

                                <Button
                                    type="submit"
                                    className={classes.primaryButtonContained}
                                    variant="contained"
                                    sx={{
                                        mt: 3,
                                        mb: 2,
                                        mr: 3,
                                        backgroundColor: '#31CACA',
                                        '&:hover': {backgroundColor: '#27a2a2'}
                                    }}
                                >
                                    Save
                                </Button>
                                {id &&
                                    <Button
                                        onClick={handleDelete}
                                        variant="contained"
                                        color={'error'}
                                        sx={{mt: 3, mb: 2, backgroundColor: '#F33535'}}
                                    >
                                        Delete
                                    </Button>
                                }

                            </Box>
                        </Box>
                    </Modal>

                    <div style={{height: '60vh', width: '100%'}}>
                        <DataGrid
                            className={classes.dataGrid}
                            onCellClick={(params, event) => {
                                event.defaultMuiPrevented = true;
                            }}
                            onRowClick={(params, event) => {
                                setModalTitle("Edit Item")
                                setText(params.row.text)
                                setId(params.row.id)
                                setSubmitType("update")
                                setItemType(params.row.typeCode)
                                setDescription(params.row.description)
                                handleOpen()
                            }}
                            rows={data}
                            columns={columns}
                            pageSize={7}
                            rowsPerPageOptions={[7]}
                            components={{}}
                        />
                    </div>

                </Sidebar>
            ) : (
                logged &&
               ( <>
                    <h2>403 - Forbidden</h2>
                    <div>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                </>)
            )}

        </>
    )

}
export default ItemsList

export async function getServerSideProps(context) {

    const {req} = context;

    const {token} = getAppCookies(req);
    const profile = token ? verifyToken(token.split(' ')[1]) : '';

    try {
        const items = await prisma.item.findMany(
            {
                include: {
                    type: true,
                },
            }
        )
        const types = await prisma.itemType.findMany()
        return {
            props: {
                profile,
                items: JSON.parse(JSON.stringify(items)),
                types: JSON.parse(JSON.stringify(types))
            }
        }
    } catch (err) {
        return {
            props: {
                profile,
                items: [],
                types: []
            }
        }
    }

}

