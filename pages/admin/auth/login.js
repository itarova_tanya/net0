import * as React from 'react';
import {useRouter} from 'next/router';
import {Container, Link, Button, Box, Typography, TextField, Checkbox, FormControlLabel} from "@mui/material";
import {ThemeProvider, withStyles} from "@mui/styles";
import {createTheme} from "@mui/material/styles";
import Cookies from 'js-cookie';

/* middleware */
import {
    absoluteUrl,
    getAppCookies,
    verifyToken,
    setLogout,
} from '../../../middleware/utils';
import axios from "axios";

const emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,2|3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const FORM_DATA_LOGIN = {
    email: {
        value: '',
        label: 'Email',
        min: 10,
        max: 36,
        required: true,
        validator: {
            regEx: emailRegEx,
            error: 'Please insert valid email',
        },
    },
    password: {
        value: '',
        label: 'Password',
        min: 6,
        max: 36,
        required: true,
        validator: {
            regEx: /^[a-z\sA-Z0-9\W\w]+$/,
            error: 'Please insert valid password',
        },
    },
};

const theme = createTheme();

const StylesTextField = withStyles({
    root: {
        '& label.Mui-focused': {
            color: 'black',
        },
        '& .MuiOutlinedInput-root': {
            '&.Mui-focused fieldset': {
                borderColor: 'unset',
                borderWidth: 1
            },
        },
    },
})(TextField);

export default function Login(props) {

    const router = useRouter();
    const {profile} = props;
    const [stateFormData, setStateFormData] = React.useState(FORM_DATA_LOGIN);
    const [stateFormError, setStateFormError] = React.useState([]);
    const [stateFormValid, setStateFormValid] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [stateFormMessage, setStateFormMessage] = React.useState({});


    async function onSubmit(e) {
        e.preventDefault();

        let data = {...stateFormData};

        /* email */
        data = {...data, email: data.email.value || ''};
        /* password */
        data = {...data, password: data.password.value || ''};

        /* validation handler */
        const isValid = validationHandler(stateFormData);

        if (isValid) {
            let res = await axios.post(`http://localhost:3000/api/auth/login`, data, {
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
            });
            // const loginApi = await fetch(`http://localhost:3000/api/auth/login`, {
            //     method: 'POST',
            //     headers: {
            //         Accept: 'application/json',
            //         'Content-Type': 'application/json',
            //     },
            //     body: JSON.stringify(data),
            // }).catch(error => {
            //     console.error('Error:', error);
            // });
            // let result = await loginApi.json();
            let result = res.data;
            if (result.success && result.token) {
                Cookies.set('token', result.token);
                router.push('/admin');
            } else {
                setStateFormMessage(result);
            }
            setLoading(false);
        }
    }

    function validationHandler(states, e) {
        const input = (e && e.target.name) || '';
        const errors = [];
        let isValid = true;

        if (input) {
            if (states[input].required) {
                if (!states[input].value) {
                    errors[input] = {
                        hint: `${states[e.target.name].label} required`,
                        isInvalid: true,
                    };
                    isValid = false;
                }
            }
            if (
                states[input].value &&
                states[input].min > states[input].value.length
            ) {
                errors[input] = {
                    hint: `Field ${states[input].label} min ${states[input].min}`,
                    isInvalid: true,
                };
                isValid = false;
                setEmailIsValid(false)
            }
            if (
                states[input].value &&
                states[input].max < states[input].value.length
            ) {
                errors[input] = {
                    hint: `Field ${states[input].label} max ${states[input].max}`,
                    isInvalid: true,
                };
                isValid = false;
            }
            if (
                states[input].validator !== null &&
                typeof states[input].validator === 'object'
            ) {
                if (
                    states[input].value &&
                    !states[input].validator.regEx.test(states[input].value)
                ) {
                    errors[input] = {
                        hint: states[input].validator.error,
                        isInvalid: true,
                    };
                    isValid = false;
                }
            }
        } else {
            Object.entries(states).forEach(item => {
                item.forEach(field => {
                    errors[item[0]] = '';
                    if (field.required) {
                        if (!field.value) {
                            errors[item[0]] = {
                                hint: `${field.label} required`,
                                isInvalid: true,
                            };
                            isValid = false;
                        }
                    }
                    if (field.value && field.min >= field.value.length) {
                        errors[item[0]] = {
                            hint: `Field ${field.label} min ${field.min}`,
                            isInvalid: true,
                        };
                        isValid = false;
                    }
                    if (field.value && field.max <= field.value.length) {
                        errors[item[0]] = {
                            hint: `Field ${field.label} max ${field.max}`,
                            isInvalid: true,
                        };
                        isValid = false;
                    }
                    if (field.validator !== null && typeof field.validator === 'object') {
                        if (field.value && !field.validator.regEx.test(field.value)) {
                            errors[item[0]] = {
                                hint: field.validator.error,
                                isInvalid: true,
                            };
                            isValid = false;
                        }
                    }
                });
            });
        }
        if (isValid) {
            setStateFormValid(isValid);
        }
        setStateFormError({
            ...errors,
        });
        return isValid;
    }

    function handleOnClickLogout(e) {
        setLogout(e);
    }

    function onChangeHandler(e) {

        const {name, value} = e.currentTarget;

        setStateFormData({
            ...stateFormData,
            [name]: {
                ...stateFormData[name],
                value,
            },
        });

        /* validation handler */
        // validationHandler(stateFormData, e);

    }

    React.useEffect(() => {
        if (profile) {
            router.push('/admin')
        }
    }, []);

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="xs">
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >
                    <Typography component="h1" variant="h5">
                        Login
                    </Typography>

                    {stateFormMessage.status === "error" && (
                        <h4 className="warning text-center" style={{color: "red"}}>{stateFormMessage.error}</h4>
                    )}
                    <Box component='form' sx={{mt: 1}} onSubmit={onSubmit}>
                        <StylesTextField
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="off"
                            autoFocus
                            onChange={onChangeHandler}
                            value={stateFormData.email.value}
                        />
                        {stateFormError.email && (
                            <span className="warning" style={{color: "red"}}>{stateFormError.email.hint}</span>
                        )}
                        <StylesTextField
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="off"
                            onChange={onChangeHandler}
                            value={stateFormData.email.password}
                        />
                        {stateFormError.password && (
                            <span className="warning" style={{color: "red"}}>{stateFormError.password.hint}</span>
                        )}
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{
                                mt: 3, mb: 2,
                                bgcolor: "#31CACA",
                                '&:hover': {
                                    bgcolor: "#31CACA"
                                }
                            }}
                        >
                            Login
                        </Button>

                        <Link href="#" variant="body2">
                            Forgot password?
                        </Link>

                    </Box>

                </Box>
            </Container>
        </ThemeProvider>
    )
}


export async function getServerSideProps(context) {

    const {req} = context;
    const {origin} = absoluteUrl(req);

    const baseApiUrl = `${origin}/api`;

    const {token} = getAppCookies(req);
    const profile = token ? verifyToken(token.split(' ')[1]) : '';
    return {
        props: {
            baseApiUrl,
            profile,
        },
    };
}


