import Sidebar from "../../../components/Sidebar";
import DataList from "../../../components/DataList";
import prisma from "/utils/PrismaClient"


const Types = ({types}) => {

    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            flex: 0.2,
        },
        {
            field: 'name',
            headerName: 'Name',
            flex: 1,
            editable: false,
        },
        {
            field: 'code',
            headerName: 'Code',
            flex: 1,
            editable: false,
        },
    ];

    return (
        <Sidebar>
            <h1>Types List</h1>
            <DataList
                rows={types}
                columns={columns}
            >
            </DataList>
        </Sidebar>
    )
}
export async function getServerSideProps() {

    const types = await prisma.itemType.findMany()
    return {props: {types: JSON.parse(JSON.stringify(types))}}
}

export default Types