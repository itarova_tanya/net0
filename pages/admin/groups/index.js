import Sidebar from "../../../components/Sidebar";
import * as React from 'react';
import prisma from "/utils/PrismaClient"
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import AddIcon from "@mui/icons-material/Add";
import Modal from '@mui/material/Modal';
import {useRouter} from 'next/router'
import {Link, FormControl, FormGroup, Grid, TextareaAutosize, TextField} from "@mui/material";
import styles from "../../../utils/styles";
import axios from "axios";
import {DataGrid} from "@mui/x-data-grid";

/* middleware */
import {
    absoluteUrl,
    getAppCookies,
    verifyToken,
    setLogout,
} from '../../../middleware/utils';


const GroupsList = (props) => {
    const router = useRouter()

    const classes = styles()

    const {profile} = props;

    const [loggedAdmin, setLoggedAdmin] = React.useState(false)
    const [logged, setLogged] = React.useState(false)
    const [open, setOpen] = React.useState(false);
    const [modalTitle, setModalTitle] = React.useState(null);
    const [data, setData] = React.useState(props.groups);
    const [name, setName] = React.useState(null);
    const [id, setId] = React.useState(null);
    const [formIsValid, setFormIsValid] = React.useState(true);
    const [formError, setFormError] = React.useState(null);
    const [description, setDescription] = React.useState(null);
    const [submitType, setSubmitType] = React.useState("create");



    React.useEffect(() => {
        if (profile) {
            setLogged(true)
            if (profile.isAdmin) {
                setLoggedAdmin(true)
            }
        } else {
            router.push('/admin/auth/login')
        }
    }, [])

    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            flex: 0.5,
        },
        {
            field: 'name',
            headerName: 'Name',
            flex: 1,
            editable: false,
        },
        {
            field: 'description',
            headerName: 'Description',
            flex: 1,
            editable: false,
        },
    ];

    const style = {
        position: 'absolute',
        top: '30%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: '50vw',
        bgcolor: 'background.paper',
        border: '1px solid #000',
        boxShadow: 12,
        p: 4,
    };


    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const handleModalSubmit = (event) => {
        event.preventDefault();

        if ((event.target.name.value).trim() && (event.target.description.value).trim()) {
            switch (submitType) {
                case "update":
                    axios
                        .put('/api/groups/' + id, {
                            name: (event.target.name.value).trim(),
                            description: (event.target.description.value.trim())
                        })
                        .then((response) => {

                            setOpen(false)
                            setData(response.data)
                            setFormIsValid(true)
                        })
                    break
                case "create":
                    axios
                        .post('/api/groups', {
                            name: (event.target.name.value).trim(),
                            description: (event.target.description.value.trim())
                        })
                        .then((response) => {

                            setOpen(false)
                            setData(response.data)
                        })
                    break
                default:
                    break

            }
        } else {
            setFormIsValid(false)
            setFormError("Enter valid values!")
        }

    }

    const handleDelete = (event) => {
        axios
            .delete('/api/groups/' + id)
            .then((response) => {
                setOpen(false)
                setData(response.data)
                setFormIsValid(true)
            })
    }

    function handleOnClickLogout(e) {
        setLogout(e);
    }

    return (
        <>
            {loggedAdmin ? (

                <Sidebar>
                    <div style={{textAlign: "right"}}>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                    <h1>Groups List</h1>
                    <Button
                        className={classes.primaryButtonOutlined}
                        onClick={
                            () => {
                                setModalTitle("Add new group")
                                setName(null)
                                setDescription(null)
                                setId(null)
                                setSubmitType("create")
                                handleOpen()
                            }
                        }
                        variant="outlined"
                        startIcon={<AddIcon/>}
                    >
                        new Group
                    </Button>
                    <p></p>
                    <Modal
                        open={open}
                        position='absolute'
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Typography id="modal-modal-title" variant="h6" component="h2">
                                {modalTitle}
                            </Typography>

                            {!formIsValid &&
                                <Typography
                                    paragraph={true}
                                    sx={{
                                        color: 'red',
                                        mt: 1,
                                        mb: 0
                                    }}
                                >
                                    {formError}
                                </Typography>
                            }

                            <Box component="form" noValidate onSubmit={handleModalSubmit}>
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="name"
                                    label="Name"
                                    name="name"
                                    autoFocus
                                    autoComplete="off"
                                    defaultValue={name}
                                />
                                <TextField
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="description"
                                    label="Description"
                                    name="description"
                                    autoComplete="off"
                                    multiline={true}
                                    minRows={2}
                                    defaultValue={description}

                                />
                                <Button
                                    type="submit"
                                    className={classes.primaryButtonContained}
                                    variant="contained"
                                    sx={{
                                        mt: 3,
                                        mb: 2,
                                        mr: 3,
                                        backgroundColor: '#31CACA',
                                        '&:hover': {backgroundColor: '#27a2a2'}
                                    }}
                                >
                                    Save
                                </Button>
                                {id &&
                                    <Button
                                        onClick={handleDelete}
                                        variant="contained"
                                        color={'error'}
                                        sx={{mt: 3, mb: 2, backgroundColor: '#F33535'}}
                                    >
                                        Delete
                                    </Button>
                                }

                            </Box>
                        </Box>
                    </Modal>

                    <div style={{height: '60vh', width: '100%'}}>
                        <DataGrid
                            className={classes.dataGrid}
                            onCellClick={(params, event) => {
                                event.defaultMuiPrevented = true;
                            }}
                            onRowClick={(params, event) => {
                                setModalTitle("Edit group")
                                setName(params.row.name)
                                setId(params.row.id)
                                setSubmitType("update")
                                setDescription(params.row.description)
                                handleOpen()
                            }}
                            rows={data}
                            columns={columns}
                            pageSize={7}
                            rowsPerPageOptions={[7]}
                            components={{}}
                        />
                    </div>

                </Sidebar>

            ) : (
                logged &&
               ( <>
                    <h2>403 - Forbidden</h2>
                    <div>
                        <a href="#" onClick={e => handleOnClickLogout(e)}>
                            &bull; Logout
                        </a>
                    </div>
                </>)
            )}
        </>
    )
}

export default GroupsList

export async function getServerSideProps(context) {

    const {req} = context;

    const {token} = getAppCookies(req);
    const profile = token ? verifyToken(token.split(' ')[1]) : '';

    let groups;
    try {
        groups = await prisma.group.findMany()

    } catch (err) {

    }

    return {
        props: {
            profile,
            groups: JSON.parse(JSON.stringify(groups)),
        },
    };

}