import {makeStyles} from "@mui/styles";

const signInStyle = makeStyles({
    signInButton: {
        color: '#ffffff',
        backgroundColor: '#31CACA',
    }
})

export default signInStyle;
